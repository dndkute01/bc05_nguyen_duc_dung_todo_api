const BASE_URL = "https://63661a02f711cb49d108ded6.mockapi.io";

function fetchAllTodo() {
  turnOnLoading();
  // render all todos service
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      // console.log("res: ", res.data);

      renderTodosList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

// running when page loading
fetchAllTodo();
// remove todos service
function removeTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      // recall get api todo
      fetchAllTodo();
      console.log("res: ", res);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

function addTodo() {
  turnOnLoading();
  var data = getInfoForm();

  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: false,
  };
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}
// ------------
var idEdited = null;
// --------------

function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      // show information on form
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;

      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}

function updatTodo() {
  turnOnLoading();
  let data = getInfoForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
